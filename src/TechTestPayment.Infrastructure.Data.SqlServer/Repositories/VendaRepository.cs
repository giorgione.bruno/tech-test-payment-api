﻿using System;
using System.Collections.Generic;
using System.Linq;
using TechTestPayment.Domain.Entities;
using TechTestPayment.Domain.Enums;
using TechTestPayment.Domain.Interfaces.Repositories;

namespace TechTestPayment.Infrastructure.Data.SqlServer.Repositories
{
    public class VendaRepository : IVendaRepository
    {
        private static List<Venda> vendasTable = null;
        public VendaRepository()
        {
            if (vendasTable == null)
                vendasTable = new List<Venda>();
        }

        public Venda Obter(long id)
        {
            return vendasTable.FirstOrDefault(v => v.Id == id);
        }

        public void Registrar(Venda venda)
        {
            vendasTable.Add(venda);
        }
        public void Atualizar(Venda venda)
        {
            var vendaObj = vendasTable.FirstOrDefault(x => x.Id == venda.Id);
            vendaObj.Status = venda.Status;
            vendaObj.DataAtualizacao = venda.DataAtualizacao;

        }
    }
}
