﻿namespace TechTestPayment.Domain.Enums
{
    public enum VendaStatus
    {
        AguardandoPagamento = 1,
        PagamentoAprovado,
        EnviadoTransportadora,
        Entregue,
        Cancelada
    }
}
