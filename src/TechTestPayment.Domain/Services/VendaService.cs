﻿using System;
using TechTestPayment.CrossCutting.Exceptions;
using TechTestPayment.Domain.Entities;
using TechTestPayment.Domain.Enums;
using TechTestPayment.Domain.Interfaces.Repositories;
using TechTestPayment.Domain.Interfaces.Services;

namespace TechTestPayment.Domain.Services
{
    public class VendaService : IVendaService
    {
        private readonly IVendaRepository _vendaRepository;

        public VendaService(IVendaRepository vendaRepository)
        {
            _vendaRepository = vendaRepository;
        }

        public Venda Obter(long id)
        {
            return _vendaRepository.Obter(id);
        }

        public void Registrar(Venda venda)
        {
            if (venda == null)
                throw new ArgumentException($"Parametro {nameof(venda)} inválido.");

            if (venda.Pedido?.PedidoItems == null || venda.Pedido?.PedidoItems.Count == 0)
                throw new BusinessException("Nenhum item informado no pedido");

            venda.Status = VendaStatus.AguardandoPagamento;
            venda.DataVenda = DateTime.Now;

            _vendaRepository.Registrar(venda);
        }

        public void AtualizarStatus(long id, VendaStatus status)
        {
            var venda = Obter(id);
            if (venda == null)
                throw new BusinessException($"Venda não encontrada com o id {id}.");

            ValidarMudancaStatus(venda.Status, status);

            venda.Status = status;
            venda.DataAtualizacao = DateTime.Now;

            _vendaRepository.Atualizar(venda);
        }

        private void ValidarMudancaStatus(VendaStatus statusAtual, VendaStatus statusNovo)
        {
            bool atualizacaoPermitida = false;

            if (statusAtual == VendaStatus.AguardandoPagamento)
            {
                if (statusNovo == VendaStatus.PagamentoAprovado ||
                    statusNovo == VendaStatus.Cancelada)
                    atualizacaoPermitida = true;

            }
            else if (statusAtual == VendaStatus.PagamentoAprovado)
            {
                if (statusNovo == VendaStatus.EnviadoTransportadora ||
                   statusNovo == VendaStatus.Cancelada)
                    atualizacaoPermitida = true;
            }

            else if (statusAtual == VendaStatus.EnviadoTransportadora)
            {
                if (statusNovo == VendaStatus.Entregue)
                    atualizacaoPermitida = true;
            }

            if (!atualizacaoPermitida)
                throw new BusinessException($"Atualização de '{statusAtual}' para '{statusNovo}' não permitida.");
        }


    }
}
