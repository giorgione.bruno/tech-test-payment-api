﻿namespace TechTestPayment.Domain.Entities
{
    public class Vendedor : EntityBase
    {
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
