﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechTestPayment.Domain.Entities
{
    public class Pedido : EntityBase
    {
        public string FormaPagamento { get; set; }
        public string NumeroCartao { get; set; }
        public string ValidadeCartao { get; set; }

        public List<PedidoItem> PedidoItems { get; set; }
    }
}
