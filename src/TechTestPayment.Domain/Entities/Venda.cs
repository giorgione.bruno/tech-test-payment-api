﻿using System;
using TechTestPayment.Domain.Enums;

namespace TechTestPayment.Domain.Entities
{
    public class Venda : EntityBase
    {
        public long VendedorId { get; set; }
        public long PedidoId { get; set; }
        public DateTime DataVenda { get; set; }
        public DateTime DataAtualizacao { get; set; }
        public VendaStatus Status { get; set; }

        public Vendedor Vendedor { get; set; }
        public Pedido Pedido { get; set; }
    }
}
