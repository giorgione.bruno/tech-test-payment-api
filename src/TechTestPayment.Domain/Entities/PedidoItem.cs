﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechTestPayment.Domain.Entities
{
    public class PedidoItem : EntityBase
    {
       public long PedidoId { get; set; }
       public long ProdutoId { get; set; }
       public int Quantidade { get; set; }
    }
}
