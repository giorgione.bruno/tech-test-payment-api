﻿using System;

namespace TechTestPayment.Domain.Entities
{
    public class Produto : EntityBase
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public decimal Preco { get; set; }
        public DateTime DataCadastro { get; set; }
        public bool Ativo { get; set; }
    }
}
