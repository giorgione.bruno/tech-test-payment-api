﻿using System;
using TechTestPayment.Domain.Entities;
using TechTestPayment.Domain.Enums;

namespace TechTestPayment.Domain.Interfaces.Repositories
{
    public interface IVendaRepository
    {
        Venda Obter(long id);
        void Registrar(Venda venda);
        void Atualizar(Venda venda);
    }
}
