﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.Entities;
using TechTestPayment.Domain.Enums;

namespace TechTestPayment.Domain.Interfaces.Services
{
    public interface IVendaService
    {
        Venda Obter(long id);
        void Registrar(Venda venda);
        void AtualizarStatus(long id, VendaStatus status);
    }
}
