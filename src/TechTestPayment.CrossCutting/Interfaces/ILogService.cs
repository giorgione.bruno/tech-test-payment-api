﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechTestPayment.CrossCutting.Interfaces
{
    public interface ILogService
    {
        void Log(string type, string message);
    }
}
