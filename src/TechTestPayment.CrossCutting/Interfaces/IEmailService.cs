﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechTestPayment.CrossCutting.Interfaces

{
    public interface IEmailService
    {
        Task Enviar(string emailTo);
    }
}
