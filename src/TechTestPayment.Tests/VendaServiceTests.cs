using NSubstitute;
using System.Collections.Generic;
using TechTestPayment.CrossCutting.Exceptions;
using TechTestPayment.Domain.Entities;
using TechTestPayment.Domain.Enums;
using TechTestPayment.Domain.Interfaces.Repositories;
using TechTestPayment.Domain.Interfaces.Services;
using TechTestPayment.Domain.Services;
using Xunit;

namespace TechTestPayment.Testes
{
    public class VendaServiceTests
    {
        private readonly IVendaService _vendaService;
        private readonly IVendaRepository _vendaRepository;
        public VendaServiceTests()
        {
            _vendaRepository = Substitute.For<IVendaRepository>();

            _vendaService = new VendaService(_vendaRepository);
        }

        [Fact(DisplayName = "Retornar uma venda com sucesso")]
        public void Registrar_RetornarVenda_DeveRetornarSucesso()
        {
            // Arrange
            Venda venda = ObterVenda();

            _vendaRepository.Obter(venda.Id).Returns(venda);

            // Act
            var vendaObj = _vendaService.Obter(venda.Id);

            // Assert
            _vendaRepository.Received().Obter(venda.Id);
            Assert.NotNull(vendaObj);
            Assert.Equal(vendaObj, venda);
        }


        [Fact(DisplayName = "Registrar Venda")]
        public void Registrar_Venda_DeveRetornarSucesso()
        {
            // Arrange
            Venda venda = ObterVenda();

            // Act
            _vendaService.Registrar(venda);

            // Assert
            Assert.True(venda.Status == VendaStatus.AguardandoPagamento);
            _vendaRepository.ReceivedWithAnyArgs().Registrar(venda);
        }

        [Fact(DisplayName = "Venda com erro de pedido inferior a 1 ")]
        public void Adicionar_Pedido_DeveRetornarErroPedido()
        {
            // Arrange
            Venda venda = ObterVenda();
            venda.Pedido.PedidoItems = null;

            // Act/Assert
            Assert.Throws<BusinessException>(() => _vendaService.Registrar(venda));
        }

        [Theory(DisplayName = "Atualizar status com sucesso ")]
        [InlineData(VendaStatus.PagamentoAprovado)]
        [InlineData(VendaStatus.Cancelada)]
        public void Atualizar_StatusPedido_DeveRetornarSucesso(VendaStatus novoStatus)
        {
            // Arrange
            Venda venda = ObterVenda();
            venda.Status = VendaStatus.AguardandoPagamento;

            _vendaService.Obter(1).Returns(venda);

            //Act
            _vendaService.AtualizarStatus(1, novoStatus);

            //Assert
            Assert.True(venda.Status == novoStatus);
            _vendaRepository.Received().Atualizar(venda);
        }

        [Theory(DisplayName = "Retornar erro na transi��o entre os status")]
        [InlineData(VendaStatus.EnviadoTransportadora)]
        [InlineData(VendaStatus.Entregue)]
        [InlineData(VendaStatus.AguardandoPagamento)]
        public void Atualizar_StatusPedido_DeveRetornarErro(VendaStatus novoStatus)
        {
            // Arrange
            Venda venda = ObterVenda();
            venda.Status = VendaStatus.AguardandoPagamento;

            _vendaService.Obter(1).Returns(venda);

            //Act/Assert
            Assert.Throws<BusinessException>(() => _vendaService.AtualizarStatus(1, novoStatus));
        }


        private Venda ObterVenda()
        {
            Venda venda = new()
            {
                Id = 1,
                Pedido = new Pedido
                {
                    PedidoItems = new List<PedidoItem> {
                        new PedidoItem
                        {
                            ProdutoId = 1,
                            Quantidade = 1
                        }
                    }
                }
            };

            return venda;
        }
    }
}
