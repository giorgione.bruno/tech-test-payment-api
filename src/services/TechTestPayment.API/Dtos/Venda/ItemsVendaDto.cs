﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPayment.API.Dtos.Venda
{
    public class ItemsVendaDto
    {
        public long ProdutoId { get; set; }
        public int Quantidade { get; set; }
    }
}
