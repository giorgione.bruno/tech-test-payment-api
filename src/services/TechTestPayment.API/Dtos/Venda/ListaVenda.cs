﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechTestPayment.API.Dtos.Venda;
using TechTestPayment.Domain.Enums;

namespace TechTestPayment.API.Dtos
{
    public class ListaVenda
    {
        public long Id { get; set; }
        public VendedorDto Vendedor { get; set; }
        public PedidoDto Pedido { get; set; }

        public VendaStatus Status { get; set; }
    }
}
