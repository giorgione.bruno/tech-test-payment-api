﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPayment.API.Dtos.Venda
{
    public class PagamentoDto
    {
        public string FormaPagamento { get; set; }
        public string NumeroCartao { get; set; }
        public string ValidadeCartao { get; set; }
    }
}
