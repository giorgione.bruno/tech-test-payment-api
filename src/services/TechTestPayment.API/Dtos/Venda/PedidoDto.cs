﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechTestPayment.API.Dtos.Venda;

namespace TechTestPayment.API.Dtos
{
    public class PedidoDto
    {
        public long Id { get; set; }
        public PagamentoDto Pagamento { get; set; }
        public List<ItemsVendaDto> ItensPedido { get; set; }
    }
}
