﻿using System;

namespace TechTestPayment.API.Dtos
{
    public class VendedorDto
    {
        public long Id { get; set; }
        public string Cpf { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
    }
}
