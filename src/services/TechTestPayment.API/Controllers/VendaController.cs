﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Net;
using TechTestPayment.API.Dtos;
using TechTestPayment.API.Dtos.Venda;
using TechTestPayment.CrossCutting.Exceptions;
using TechTestPayment.CrossCutting.Interfaces;
using TechTestPayment.Domain.Entities;
using TechTestPayment.Domain.Enums;
using TechTestPayment.Domain.Interfaces.Services;

namespace TechTestPayment.API.Controllers
{
    [Route("api/venda")]
    [ApiController]
    public class VendaController : ControllerBase
    {
        private readonly IVendaService _vendaService;
        private readonly IEmailService _emailService;
        private readonly ILogService _logService;

        public VendaController(IVendaService vendaService,
                               IEmailService emailService,
                               ILogService logService)
        {
            _vendaService = vendaService;
            _emailService = emailService;
            _logService = logService;
        }

        [HttpGet("{id:long}")]
        public IActionResult Obter([FromRoute] long id)
        {
            var venda = _vendaService.Obter(id);
            if (venda == null)
                return NotFound(new { message = $"Nenhuma venda encontrada para o id {id}." });

            return Ok(VendaToDto(venda));
        }

        [HttpPost]
        public IActionResult Registrar([FromBody] RegistraVendaDto registraVendaDto)
        {

            try
            {
                //O map diretamente aqui na controller foi usado apenas para exemplicar.
                //Poderiamos no lugar usar AutoMapper com os mapeamentos das Dtos e entidades em uma classe isolada.
                _vendaService.Registrar(DtoToVenda(registraVendaDto));

                //Neste momento poderia ser enviado um email de forma assincrona para o vendedor informando sucesso no processo.
                //Enviar um request para uma API ou fila para um microserviço de pagamento.

                return CreatedAtAction(nameof(Obter), new { id = registraVendaDto.Id }, registraVendaDto);
            }
            //Poderiamos criar um middleware de erro, asism evitaria  'try catchs' repetitivos no código.
            catch (BusinessException bx)
            {
                return BadRequest(new { message = bx.Message });
            }
            catch (Exception ex)
            {
                _logService.Log("error", ex.Message);
                return StatusCode(500, new { message = ex.Message });
            }
        }

        [HttpPatch("{id:long}/status/{status:long}")]
        public IActionResult AtualizarStatus([FromRoute] long id, [FromRoute] VendaStatus status)
        {
            try
            {
                var venda = _vendaService.Obter(id);
                if (venda == null)
                    return NotFound(new { message = $"Nenhuma venda encontrada para o id {id}." });

                _vendaService.AtualizarStatus(id, status);

                return NoContent();

            }
            catch (BusinessException bx)
            {
                return BadRequest(new { message = bx.Message });
            }
            catch (Exception ex)
            {
                _logService.Log("error", ex.Message);
                return StatusCode(500, new { message = ex.Message });
            }
        }

        private Venda DtoToVenda(RegistraVendaDto registraVenda)
        {
            Venda venda = new()
            {
                Id = registraVenda.Id,
                Vendedor = new Vendedor
                {
                    Cpf = registraVenda.Vendedor.Cpf,
                    Nome = registraVenda.Vendedor.Nome,
                    Email = registraVenda.Vendedor.Email,
                    Telefone = registraVenda.Vendedor.Telefone
                },
                Pedido = new Pedido
                {
                    NumeroCartao = registraVenda.Pagamento.NumeroCartao,
                    FormaPagamento = registraVenda.Pagamento.FormaPagamento,
                    ValidadeCartao = registraVenda.Pagamento.ValidadeCartao,

                    PedidoItems = (from p in registraVenda.Items
                                   select new PedidoItem
                                   {
                                       ProdutoId = p.ProdutoId,
                                       Quantidade = p.Quantidade
                                   }).ToList()
                }
            };
            return venda;
        }

        private ListaVenda VendaToDto(Venda venda)
        {
            return new ListaVenda
            {
                Id = venda.Id,
                Status = venda.Status,
                Vendedor = new VendedorDto
                {
                    Id = venda.Vendedor.Id,
                    Nome = venda.Vendedor.Nome,
                    Cpf = venda.Vendedor.Cpf,
                    Email = venda.Vendedor.Email,
                    Telefone = venda.Vendedor.Telefone
                },
                Pedido = new PedidoDto
                {
                    Id = venda.Pedido.Id,
                    Pagamento = new PagamentoDto
                    {
                        FormaPagamento = venda.Pedido.FormaPagamento,
                        NumeroCartao = venda.Pedido.NumeroCartao,
                        ValidadeCartao = venda.Pedido.ValidadeCartao,
                    },
                    ItensPedido = (from p in venda.Pedido.PedidoItems
                                   select new ItemsVendaDto
                                   {
                                       ProdutoId = p.ProdutoId,
                                       Quantidade = p.Quantidade
                                   }).ToList()
                }
            };
        }
    }
}
