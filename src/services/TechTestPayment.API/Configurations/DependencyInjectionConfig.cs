﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TechTestPayment.CrossCutting;
using TechTestPayment.CrossCutting.Interfaces;
using TechTestPayment.Domain.Interfaces.Repositories;
using TechTestPayment.Domain.Interfaces.Services;
using TechTestPayment.Domain.Services;
using TechTestPayment.Infrastructure.Data.SqlServer.Repositories;

namespace TechTestPayment.API.Configurations
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencyInjection(this IServiceCollection services)
        {
            //Services - Domain
            services.AddScoped<IVendaService, VendaService>();

            //Services - Infra
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<ILogService, LogService>();

            //Repositories
            services.AddScoped<IVendaRepository, VendaRepository>();

            return services;
        }
    }
}
